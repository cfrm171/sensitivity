# sensitivity

Risk sensitivities, also referred to as Greeks, are the measure of a financial instrument’s value reaction to changes in underlying factors. The value of a financial instrument is impacted by many factors, such as interest rate, stock price, implied 